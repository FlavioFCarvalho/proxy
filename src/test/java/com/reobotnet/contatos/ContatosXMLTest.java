package com.reobotnet.contatos;

import static org.junit.Assert.*;

import org.junit.Test;

import com.reobotnet.repository.Contatos;

public class ContatosXMLTest {
	
	@Test
	public void deveRetornarContato() {
		Contatos contatos = new ContatosXML("contatos1.xml", "contatos2.xml", "contatos3.xml");
	    String nome = contatos.buscarPor("flavio@mail.com");
	    assertEquals("Flávio Carvalho", nome);
	    
	   
	}

}
